package edu.iastate.cs228.hw3;

/*
 * @author Firstname Lastname
 */

import java.util.AbstractSequentialList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Implementation of the list interface based on linked nodes that store
 * multiple items per node. Rules for adding and removing elements ensure that
 * each node (except possibly the last one) is at least half full.
 *
 * A link to the JavaDoc documentation for the interface AbstractSequentialList
 * <E> is provided next to the pdf spec on Blackboard.
 *
 * You should carefully study the complete methods given below to learn how to
 * go about implementing other methods.
 *
 * You are encouraged to introduce private methods that can be used to simplify
 * the implementation of public methods.
 */
public class ChunkyList<E extends Comparable<? super E>> extends AbstractSequentialList<E> {
    /**
     * Default number of elements that may be stored in each node.
     */
    private static final int DEFAULT_NODESIZE = 4;

    /**
     * Number of elements that can be stored in each node.
     */
    private final int nodeSize;

    /**
     * Dummy node for head. It should be private but set to public here only for
     * grading purpose. In practice, you should always make the head of a linked
     * list a private instance variable.
     */
    public Node head;

    /**
     * Dummy node for tail.
     */
    private Node tail;

    /**
     * Number of elements in the list.
     */
    private int size;

    /**
     * Constructs an empty list with the default node size.
     */
    public ChunkyList() {
	this(DEFAULT_NODESIZE);
    }

    /**
     * Constructs an empty list with the given node size.
     * 
     * @param nodeSize
     *            number of elements that may be stored in each node, must be an
     *            even number
     */
    public ChunkyList(int nodeSize) {
	if (nodeSize <= 0 || nodeSize % 2 != 0)
	    throw new IllegalArgumentException();

	// dummy nodes
	head = new Node();
	tail = new Node();
	head.next = tail;
	tail.previous = head;
	this.nodeSize = nodeSize;
    }

    /**
     * Constructor for grading only. Fully implemented.
     * 
     * @param head
     * @param tail
     * @param nodeSize
     * @param size
     */
    public ChunkyList(Node head, Node tail, int nodeSize, int size) {
	this.head = head;
	this.tail = tail;
	this.nodeSize = nodeSize;
	this.size = size;
    }

    @Override
    public boolean add(E item) {
	add(size, item);
	return true;
    }

    @Override
    public void add(int pos, E item) {
	// If the list is empty create a new node and put item at first position
	if (size == 0) {
	    createNewNode(head, tail, item);
	    size++;
	    return;
	}
	// Get the offset and change reference of the current node
	NodeInfo info = getInfo(pos);
	int offset = info.offset;
	Node current = info.node;
	if (offset == 0) {
	    if (current.previous != head && current.previous.count < (nodeSize)) {
		current.previous.addItem(item);
	    } else if (current == tail && current.previous.count == nodeSize) {
		createNewNode(current.previous, tail, item);
	    }
	} else if (current.count < nodeSize) {
	    current.addItem(offset, item);
	} else {
	    split(offset, current, item);
	}

	size++;
    }

    /**
     * Gets the NodeInfo for an abstract at the given position
     * 
     * @param pos
     *            The index of the abstract
     * @return the NodeInfo corresponding to the abstract at index _pos_
     */
    private NodeInfo getInfo(int pos) {
	Node current = head.next;
	while (current != tail) {
	    if (pos < current.count && pos < nodeSize) {
		return new NodeInfo(current, pos);
	    } else {
		pos -= current.count;
	    }
	    current = current.next;
	}
	return new NodeInfo(current, 0);
    }

    /**
     * Creates a new Node with given item in between given nodes.
     * 
     * @param previous
     *            The node to come before this node
     * @param next
     *            The node to come after this node
     * @param item
     *            The item to fill this node with
     */
    private void createNewNode(Node previous, Node next, E item) {
	Node current = new Node();
	// Makes sure that if a null item is passed through, the count of the
	// new node will not be affected
	if (item != null)
	    current.addItem(item);

	previous.next = current;
	current.previous = previous;
	current.next = next;
	next.previous = current;
    }

    /**
     * Deletes the given node
     * 
     * @param current
     *            The node to delete from the ChunkyList
     */
    private void deleteNode(Node current) {
	current.previous.next = current.next;
	current.next.previous = current.previous;
    }

    /**
     * Splits a given node at given offset and adds a given item.
     * 
     * @param offset
     *            The offset to add _item_
     * @param current
     *            The node to split
     * @param item
     *            The item to add after splitting
     */
    private void split(int offset, Node current, E item) {
	// Create a new node
	createNewNode(current, current.next, null);
	// transfer last m/2 items over (moving from end to middle due to the
	// shifting nature of removeItem
	for (int i = nodeSize - 1; i >= (nodeSize / 2); i--) {
	    current.next.addItem(0, current.data[i]);
	    current.removeItem(i);
	}
	if (offset <= (nodeSize / 2)) {
	    current.addItem(offset, item);
	} else if (offset > (nodeSize / 2)) {
	    current.next.addItem(offset - (nodeSize / 2), item);
	}

    }

    @Override
    public E remove(int pos) {
	NodeInfo info = getInfo(pos);
	int offset = info.offset;
	Node current = info.node;
	E removedItem = null;
	if (current.next == tail && current.count == 1) {
	    removedItem = current.data[0];
	    deleteNode(current);

	} else if (current.next == tail || current.count > (nodeSize / 2)) {
	    removedItem = current.data[offset];
	    current.removeItem(offset);
	} else {
	    removedItem = current.data[offset];
	    current.removeItem(offset);
	    if (current.next.count > (nodeSize / 2)) {
		current.addItem(current.next.data[0]);
		current.next.removeItem(0);
	    } else {
		for (int i = 0; i < current.next.count; i++) {
		    current.addItem(current.next.data[i]);
		}
		deleteNode(current.next);
	    }
	}
	size--;
	return removedItem;
    }

    /**
     * Removes each element (from the list) that is equal to an element in an
     * array arr[]. You should use an efficient algorithm to implement this
     * method. One efficient implementation is given as follows. Sort the array
     * arr[] with Arrays.sort(). Use a ListIterator object to access each
     * element in this list. If the element is found in the array arr[] with
     * Arrays.binarySearch(), then remove the element from the list.
     * 
     * @param arr
     *            array of (unsorted) elements
     */
    public void removeAll(E[] arr) {
	Arrays.sort(arr);
	ListIterator<E> listIt = listIterator();
	while (listIt.hasNext()) {
	    E current = listIt.next();
	    if (Arrays.binarySearch(arr, current) >= 0) {
		listIt.remove();
	    }
	}
    }

    @Override
    public int size() {
	return size;
    }

    @Override
    public Iterator<E> iterator() {
	return new ChunkyIterator();
    }

    @Override
    public ListIterator<E> listIterator() {
	return new ChunkyListIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
	return new ChunkyListIterator(index);
    }

    /**
     * Returns a string representation of this list showing the internal
     * structure of the nodes.
     */
    public String toStringInternal() {
	return toStringInternal(null);
    }

    /**
     * Returns a string representation of this list showing the internal
     * structure of the nodes and the position of the iterator.
     *
     * This complete example illustrates how a method in this data structure is
     * implemented. You should study this code carefully and use this method to
     * show the contents of the list every time you implement and use a new
     * method.
     *
     * @param iter
     *            an iterator for this list
     */
    public String toStringInternal(ListIterator<E> iter) {
	int count = 0;
	int position = -1;
	if (iter != null) {
	    position = iter.nextIndex();
	}

	StringBuilder sb = new StringBuilder();
	sb.append('[');
	Node current = head.next;
	while (current != tail) {
	    sb.append('(');
	    for (int i = 0; i < nodeSize; ++i) {
		if (i > 0)
		    sb.append(", ");
		E data = current.data[i];
		if (data == null) {
		    sb.append("-");
		} else {
		    if (position == count) {
			sb.append("| ");
			position = -1;
		    }
		    sb.append(data.toString());
		    ++count;

		    // iterator at end
		    if (position == size && count == size) {
			sb.append(" |");
			position = -1;
		    }
		}
	    }
	    sb.append(')');
	    current = current.next;
	    if (current != tail)
		sb.append(", ");
	}
	sb.append("]");
	return sb.toString();
    }

    /**
     * Node type for this list. Each node holds a maximum of nodeSize elements
     * in an array. Empty slots are null.
     */
    private class Node {
	/**
	 * Array of actual data elements.
	 */
	// Unchecked warning unavoidable.
	public E[] data = (E[]) new Comparable[nodeSize];

	/**
	 * Link to next node.
	 */
	public Node next;

	/**
	 * Link to previous node;
	 */
	public Node previous;

	/**
	 * Index of the next available offset in this node, also equal to the
	 * number of elements in this node.
	 */
	public int count;

	/**
	 * Adds an item to this node at the first available offset.
	 * Precondition: count < nodeSize
	 * 
	 * @param item
	 *            element to be added
	 */
	void addItem(E item) {
	    if (count >= nodeSize) {
		return;
	    }
	    data[count++] = item;
	    // useful for debugging
	    // System.out.println("Added " + item.toString() + " at index " +
	    // count + " to node " + Arrays.toString(data));
	}

	/**
	 * Adds an item to this node at the indicated offset, shifting elements
	 * to the right as necessary.
	 * 
	 * Precondition: count < nodeSize
	 * 
	 * @param offset
	 *            array index at which to put the new element
	 * @param item
	 *            element to be added
	 */
	void addItem(int offset, E item) {
	    if (count >= nodeSize) {
		return;
	    }
	    for (int i = count - 1; i >= offset; --i) {
		data[i + 1] = data[i];
	    }
	    ++count;
	    data[offset] = item;
	    // useful for debugging
	    // System.out
	    // .println("Added " + item.toString() + " at index " + offset + "
	    // to node: " + Arrays.toString(data));
	}

	/**
	 * Deletes an element from this node at the indicated offset, shifting
	 * elements left as necessary. Precondition: 0 <= offset < count
	 * 
	 * @param offset
	 */
	void removeItem(int offset) {
	    E item = data[offset];
	    for (int i = offset + 1; i < nodeSize; ++i) {
		data[i - 1] = data[i];
	    }
	    data[count - 1] = null;
	    --count;
	}
    }

    /**
     * This class provides a wrapper that defines a position of any abstract E
     * 
     * @author Miles Lucas mdlucas@iastate.edu
     *
     */
    private class NodeInfo {

	/**
	 * The node that this info refers to
	 */
	public Node node;

	/**
	 * The offset within the node that this info refers to
	 */
	public int offset;

	/**
	 * Creates a NodeInfo with given node and offset
	 * 
	 * @param node
	 *            The node corresponding to the abstract
	 * @param offset
	 *            The offset corresponding to the abstract
	 */
	public NodeInfo(Node node, int offset) {
	    this.node = node;
	    this.offset = offset;
	}
    }

    /**
     * This class is an iterator designed for the ChunkyList
     * 
     * @author Miles Lucas mdlucas@iastate.edu
     *
     */
    private class ChunkyIterator implements Iterator<E> {

	/**
	 * The current index of the iterator
	 */
	private int currentIndex;

	public ChunkyIterator() {
	    currentIndex = 0;
	}

	@Override
	public boolean hasNext() {
	    return currentIndex < size;
	}

	@Override
	public E next() {
	    NodeInfo info = getInfo(currentIndex);
	    currentIndex++;
	    return info.node.data[info.offset];
	}

    }

    /**
     * This class is a ListIterator designed for the ChunkyList
     * 
     * @author Miles Lucas mdlucas@iastate.edu
     *
     */
    private class ChunkyListIterator implements ListIterator<E> {
	// constants you possibly use ...

	// instance variables ...
	private NodeInfo current;
	private int pos;
	private int lastPos;

	/**
	 * Default constructor
	 */
	public ChunkyListIterator() {
	    this(0);
	}

	/**
	 * Constructor finds node at a given position.
	 * 
	 * @param pos
	 */
	public ChunkyListIterator(int pos) {
	    current = getInfo(pos);
	    this.pos = pos;
	    lastPos = -1;
	}

	@Override
	public boolean hasNext() {
	    return pos < size;
	}

	@Override
	public E next() {
	    if (!hasNext())
		throw new NoSuchElementException();
	    E next = current.node.data[current.offset];
	    lastPos = pos;
	    pos++;
	    current = getInfo(pos);
	    return next;
	}

	@Override
	public void remove() {
	    if (lastPos == -1) {
		throw new IllegalStateException("Can only remove once after calling next() or previous()");
	    }

	    ChunkyList.this.remove(lastPos);
	    if (lastPos < pos) {
		pos--;
	    }

	    lastPos = -1;
	    current = getInfo(pos);
	}

	@Override
	public void add(E item) {
	    ChunkyList.this.add(pos, item);
	    pos++;
	    lastPos = -1;
	}

	@Override
	public boolean hasPrevious() {
	    return pos > 0;
	}

	@Override
	public int nextIndex() {
	    return pos;
	}

	@Override
	public E previous() {
	    if (!hasPrevious())
		throw new NoSuchElementException();
	    pos--;
	    current = getInfo(pos);
	    lastPos = pos;
	    E previous = current.node.data[current.offset];

	    return previous;

	}

	@Override
	public int previousIndex() {
	    return pos - 1;
	}

	@Override
	public void set(E item) {
	    if (lastPos == -1) {
		throw new IllegalStateException("Can only set once after calling next() or previous()");
	    }
	    ChunkyList.this.remove(lastPos);
	    ChunkyList.this.add(lastPos, item);
	}

    }

}
