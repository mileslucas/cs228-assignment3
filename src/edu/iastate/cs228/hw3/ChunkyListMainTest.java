package edu.iastate.cs228.hw3;

import java.util.ListIterator;

public class ChunkyListMainTest {

    public static void main(String[] args) {
//	test1();
	test2();
    }

	private static void test1() {
		ChunkyList<String> list = new ChunkyList<String>();
		list.add("a");
		list.add("b");
		list.add("-");
		list.add("-");
		list.add("c");
		list.add("d");
		list.add("e");
		list.remove(3);
		list.remove(2);
		list.add("v");
		list.add("w");
		list.add(2, "x");	
		list.add(2, "y");
		list.add(2, "z");
		list.remove(9);
		list.remove(3);
		list.remove(3);
		list.remove(5);
		list.remove(3);
		System.out.println(list.toStringInternal());
	}

	private static void test2() {
		ChunkyList<Integer> list = new ChunkyList<Integer>();
		ListIterator<Integer> listIt = list.listIterator();
		listIt.add(0);
		listIt.add(1);
		listIt.add(1);
		listIt.add(1);
		Integer[] remove = {1};
		list.removeAll(remove);
		System.out.println(listIt.nextIndex());
		System.out.println(list.toStringInternal(listIt));
	}

}
