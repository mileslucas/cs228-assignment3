package edu.iastate.cs228.hw3;

import static org.junit.Assert.*;

import java.util.ListIterator;

import org.junit.Before;
import org.junit.Test;

public class ChunkyListTest2 {

    Integer[] stuff = new Integer[100];

    @Before
    public void setup() {
	for (int x = 0; x < 100; x++) {
	    stuff[x] = new Integer(x);
	}

    }

//    @Test
//    public void addTest0() {
//	ChunkyList list = new ChunkyList<String>();
//	list.add(stuff[0]);
//	// System.out.println(list.toStringInternal());
//	assertEquals(0, ((Integer) list.tail.previous.data[0]).intValue());
//
//    }

//    @Test
//    public void addTest1() {
//	ChunkyList list = new ChunkyList<String>();
//	list.add(stuff[0]);
//	// System.out.println(list.toStringInternal());
//	list.add(stuff[1]);
//	// System.out.println(list.toStringInternal());
//	boolean pass = (((Integer) list.tail.previous.data[0]).intValue() == 0);
//	boolean pas = (((Integer) list.tail.previous.data[1]).intValue() == 1);
//	assertEquals(true, pass && pas);
//    }

//    @Test
//    public void addTest2() {
//	ChunkyList list = new ChunkyList<String>();
//	list.add(stuff[0]);
//	// System.out.println(list.toStringInternal());
//	list.add(stuff[1]);
//	// System.out.println(list.toStringInternal());
//	list.add(stuff[2]);
//	// System.out.println(list.toStringInternal());
//	list.add(stuff[3]);
//	// System.out.println(list.toStringInternal());
//	list.add(stuff[4]);
//	// System.out.println(list.toStringInternal());
//
//	boolean pass = ((Integer) list.tail.previous.data[0]).intValue() == 4;
//	assertEquals(true, pass);
//    }

    @Test
    public void addTest3() {
	ChunkyList list = new ChunkyList<String>();
	for (int x = 0; x < 20; x++) {
	    list.add(stuff[x]);
	}

	assertEquals(20, list.size());
	// System.out.println(list.toStringInternal());

	// System.out.println(list.toStringInternal());

	// System.out.println(list.toStringInternal());

	// System.out.println(list.toStringInternal());
    }

    @Test
    public void hasNextTest0() {
	ChunkyList list = new ChunkyList<String>();
	for (int x = 0; x < 20; x++) {
	    list.add(stuff[x]);
	}
	ListIterator<Integer> var = list.listIterator();
	boolean pass = var.hasNext();
	assertEquals(true, pass);
    }

    @Test
    public void hasNextTest1() {
	ChunkyList list = new ChunkyList<String>();
	list.add(stuff[0]);
	list.add(stuff[1]);
	ListIterator<Integer> var = list.listIterator(1);
	assertEquals(true, var.hasNext());
    }

    @Test
    public void nextTest0() {
	ChunkyList list = new ChunkyList<String>();
	list.add(stuff[0]);
	list.add(stuff[1]);
	ListIterator<Integer> var = list.listIterator();
	assertEquals(0, var.next().intValue());
    }

    @Test
    public void nextTest1() {
	ChunkyList list = new ChunkyList<String>();
	list.add(stuff[0]);
	list.add(stuff[1]);
	ListIterator<Integer> var = list.listIterator();
	var.next();
	assertEquals(1, var.next().intValue());
    }

    @Test
    public void hasNextTest2() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	ListIterator<Integer> var = list1.listIterator();
	var.next();
	var.next();
	boolean pass = var.hasNext();
	assertEquals(false, pass);
    }

    @Test
    public void hasNextTest3() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(3);
	boolean pass = var.hasNext();
	assertEquals(true, pass);
    }

    @Test
    public void hasNextTest4() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(4);
	var.next();
	boolean pass = var.hasNext();
	assertEquals(false, pass);
    }

    @Test
    public void nextIndexTest0() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(3);
	assertEquals(3, var.nextIndex());
    }

    @Test
    public void nextIndexTest1() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator();
	var.next();
	var.next();
	int num = var.nextIndex();
	assertEquals(2, num);

    }

    @Test
    public void nextIndexTest2() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator();
	int num = var.nextIndex();
	assertEquals(0, num);
    }

    @Test
    public void nextTest3() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator();
	int num = var.next();
	assertEquals(0, num);
    }

    @Test
    public void nextTest4() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator();
	var.next();
	int num = var.next();
	assertEquals(1, num);
    }

    @Test
    public void nextIndexTest3() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(4);
	int num = var.nextIndex();
	assertEquals(4, num);
    }

    @Test
    public void nextIndexTest4() {
	ChunkyList list = new ChunkyList<String>();
	for (int x = 0; x < 20; x++) {
	    list.add(stuff[x]);
	}
	ListIterator<Integer> var = list.listIterator(19);
	var.next();
	assertEquals(list.size(), var.nextIndex());
    }

    @Test
    public void nextIndexTest5() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(2);
	assertEquals(2, var.nextIndex());

    }

    @Test
    public void previousIndexTest0() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(2);
	int num = var.previousIndex();
	assertEquals(1, num);
    }

    @Test
    public void previousIndexTest1() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(4);
	int num = var.previousIndex();
	assertEquals(3, num);
    }

    @Test
    public void previousIndexTest2() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator();
	int num = var.previousIndex();
	assertEquals(-1, num);
    }

    @Test
    public void hasPreviousTest0() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator();
	boolean yar = var.hasPrevious();
	assertEquals(false, yar);
    }

    @Test
    public void hasPreviousTest1() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(1);
	boolean yar = var.hasPrevious();
	assertEquals(true, yar);
    }

    @Test
    public void hasPreviousTest2() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(4);
	boolean yar = var.hasPrevious();
	assertEquals(true, yar);
    }

    @Test
    public void previousTest0() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(4);
	var.next();
	int num = var.previous().intValue();
	assertEquals(4, num);
    }

    @Test
    public void previousTest1() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	ListIterator<Integer> var = list1.listIterator(2);
	int num = var.next();
	int num1 = var.previous();
	boolean test = (num == num1);
	assertEquals(true, test);
    }

    @Test
    public void previousTest2() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	list1.add(stuff[5]);
	ListIterator<Integer> var = list1.listIterator(5);
	var.previous();
	var.previous();
	int num = var.next();
	assertEquals(3, num);
    }

    @Test
    public void setTest0() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	list1.add(stuff[5]);
	ListIterator<Integer> var = list1.listIterator(3);
	var.next();
	var.set(new Integer(20));
	int num = var.previous();
	assertEquals(20, num);
    }

    @Test
    public void setTest1() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	list1.add(stuff[5]);
	ListIterator<Integer> var = list1.listIterator(3);
	var.previous();
	var.set(new Integer(123));
	int num = var.next();
	assertEquals(123, num);
    }

    @Test
    public void setTest2() {
	ChunkyList list1 = new ChunkyList<String>();
	ListIterator<Integer> var = list1.listIterator();
	var.add(stuff[0]);
	var.add(stuff[1]);
	var.add(stuff[2]);
	var.add(stuff[3]);
	var.add(stuff[4]);
	var.add(stuff[5]);
	var.previous();
	var.set(new Integer(20));
	int num = var.next();
	assertEquals(20, num);
    }

    @Test
    public void addTestPosTest0() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	list1.add(stuff[5]);
	list1.add(3, new Integer(500));
	ListIterator<Integer> var = list1.listIterator(3);
	int num = var.next();
	assertEquals(500, num);
    }

    @Test
    public void addTestPosTest1() {
	ChunkyList list1 = new ChunkyList<String>();
	list1.add(stuff[0]);
	list1.add(stuff[1]);
	list1.add(stuff[2]);
	list1.add(stuff[3]);
	list1.add(stuff[4]);
	list1.add(stuff[5]);
	ListIterator<Integer> var = list1.listIterator(3);
	list1.add(6, new Integer(500));
	list1.add(7, new Integer(600));
	list1.add(8, new Integer(700));
	for (int x = 0; x < 5; x++) {
	    var.next();
	}
	int num = var.next();
	assertEquals(700, num);

    }
}
