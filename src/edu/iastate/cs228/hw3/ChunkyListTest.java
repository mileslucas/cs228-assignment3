package edu.iastate.cs228.hw3;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.ListIterator;

public class ChunkyListTest {

    private ChunkyList<String> list;
    private ListIterator<String> listIt;

    @Before
    public void initialize() {
	list = new ChunkyList<String>();
	listIt = list.listIterator();
	
    }
    
    @Test
    public void testAddAndRemove() {
	list.add("a");
	list.add("b");
	list.add("-");
	list.add("-");
	list.add("c");
	list.add("d");
	list.add("e");
	list.remove(3);
	list.remove(2);
	list.add("v");
	list.add("w");
	list.add(2, "x");	
	list.add(2, "y");
	list.add(2, "z");
	list.remove(9);
	list.remove(3);
	list.remove(3);
	list.remove(5);
	list.remove(3);
	String correct = "[(a, b, z, -), (d, v, -, -)]";
	assertEquals(correct, list.toStringInternal());
    }
    
    @Test
    public void testIterator() {
	list.add("a");
	list.add("b");
	list.add("-");
	list.add("-");
	list.add("c");
	list.add("d");
	list.add("e");
	list.remove(3);
	list.remove(2);
	
	Iterator<String> it = list.iterator();
	StringBuilder sb = new StringBuilder();
	while(it.hasNext()) {
	    sb.append(it.next());
	}
	String correct = "abcde";
	assertEquals(correct, sb.toString());
    }
    
    @Test
    public void testListIterator() {
	listIt.add("a");
	listIt.add("b");
	assertEquals(list.size(), listIt.nextIndex());
    }
    @Test
    public void testListIterator2() {
	listIt = list.listIterator(1);
	assertEquals(1, listIt.nextIndex());
    }
    
    @Test
    public void testHasNext() {
	listIt.add("a");
	listIt.add("b");
	listIt.add("c");
	assertFalse(listIt.hasNext());
    }

}
